DOMAIN=debian-tasks
TASKDESC=$(DOMAIN).desc
TASKDIR=/usr/share/blendsel
DESCDIR=tasks
VERSION=$(shell expr "`dpkg-parsechangelog 2>/dev/null |grep Version:`" : '.*Version: \(.*\)' | cut -d - -f 1)
LANGS=ar bg bn bs ca cs cy da de dz el eo es et eu fa fi fr gl gu he hi hr hu hy id it ja km ko lt lv mg mk nb ne nl nn pa pl pt_BR pt ro ru sk sl sq sv ta te th tl tr uk vi wo zh_CN zh_TW
LANGS_DESC=ar bg bn bs ca cs cy da de dz el eo es et et eu fi fr gl gu he hi hr hu id it ja km ko lt lv mg mk nb ne nl nn pa pl pt_BR pt ro ru sk sl sq sv te th tl tr uk vi wo zh_CN zh_TW
LOCALEDIR=$(DESTDIR)/usr/share/locale

all: $(TASKDESC)

$(TASKDESC): makedesc.pl $(DESCDIR)/[a-z]??*
	./doincludes.pl $(DESCDIR)
	./makedesc.pl $(DESCDIR) $(TASKDESC)

%.o: %.c
	$(COMPILE) $<

po/build_stamp:
	$(MAKE) -C po LANGS="$(LANGS)"

updatepo:
	$(MAKE) -C po update LANGS="$(LANGS)"

install:
	install -d $(DESTDIR)/usr/bin \
		$(DESTDIR)/usr/lib/blendsel/tests \
		$(DESTDIR)/usr/lib/blendsel/packages \
		$(DESTDIR)/usr/share/man/man8
	install -m 755 blendsel.pl $(DESTDIR)/usr/bin/blendsel
	install -m 755 blendsel-debconf $(DESTDIR)/usr/lib/blendsel/
	install -m 755 tests/new-install $(DESTDIR)/usr/lib/blendsel/tests/
	install -m 755 tests/debconf $(DESTDIR)/usr/lib/blendsel/tests/
	install -m 755 packages/list $(DESTDIR)/usr/lib/blendsel/packages/
	pod2man --section=8 --center "Debian specific manpage" --release $(VERSION) blendsel.pod | gzip -9c > $(DESTDIR)/usr/share/man/man8/blendsel.8.gz
#	for lang in $(LANGS); do \
#		[ ! -d $(LOCALEDIR)/$$lang/LC_MESSAGES/ ] && mkdir -p $(LOCALEDIR)/$$lang/LC_MESSAGES/; \
#		install -m 644 po/$$lang.mo $(LOCALEDIR)/$$lang/LC_MESSAGES/tasksel.mo; \
#	done

install-data:
	install -d $(DESTDIR)$(TASKDIR)/descs \
		$(DESTDIR)/usr/lib/blendsel/tests
	install -m 0644 $(TASKDESC) $(DESTDIR)$(TASKDIR)/descs
	for test in tests/*; do \
		[ "$$test" = "tests/new-install" ] && continue; \
		[ "$$test" = "tests/debconf" ] && continue; \
		install -m 755 $$test $(DESTDIR)/usr/lib/blendsel/tests/; \
	done
	for package in packages/*; do \
		[ "$$package" = "packages/list" ] && continue; \
		install -m 755 $$package $(DESTDIR)/usr/lib/blendsel/packages/; \
	done
#	for lang in $(LANGS_DESC); do \
#		[ ! -d $(LOCALEDIR)/$$lang/LC_MESSAGES/ ] && mkdir -p $(LOCALEDIR)/$$lang/LC_MESSAGES/; \
#		install -m 644 $(DESCDIR)/po/$$lang.mo $(LOCALEDIR)/$$lang/LC_MESSAGES/$(DOMAIN).mo; \
#	done

clean:
	rm -f $(TASKDESC) *~
	$(MAKE) -C po clean
